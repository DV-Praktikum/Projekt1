#include "mbed.h"
 DigitalOut myled(LED1);     //Erstellen eines Objekts vom Typ DigitalOut

 int main() {
     while(1) {
         myled = 1;          //LED1 Leuchtet
         wait(0.2);          //Zustand für 0,2 Sekunden abwarten
         myled = 0;          //LED1 wird wieder ausgeschaltet
         wait(0.2);
     }
 }