 #include "DogLCD.h"
 #include "hellombed.h"

 SPI spi(p5, NC, p7);
 DogLCD dog(spi, p17, p18, p20, p19);        //  spi, power, cs, a0, reset

 int main()
 {
     dog.init();
     // draw "hello mbed"
     dog.send_pic(pic_hellombed);
     wait(5);
 }