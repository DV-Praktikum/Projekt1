Schlussfazit
============

Diese Dokumentation soll einen Überblick zum Programmieren von Mbed-Geräten mit C++ mit Hilfe des Online- und Offlinecompilers verschaffen.

Die Mbed-Platform bietet einen angenehmen Einstieg in das Entwickeln mit eingebetteten Systemen. Gerade das Editieren und Kompilieren mit der Onlineanwendung geht sehr leicht von der Hand, zudem verfügt man dort über eine große Community, welche enorme Mengen an verschiedenen Programmbeispielen zur Verfügung stellt.

Zusammenfassend kann man sagen, dass sich das Mbed zur Realisierung kleinerer Projekte wie beispielsweise unsere Ampelanlage eignet. Denkbar wären aber auch größere Projekte wie zum Beispiel eine Steuerung zur „Office-Beleuchtung“.
Diese könnte man durch Erweiterungen wie z.B durch hinzufügen von Sensoren ,die abhängig von bestimmten Ton-Frequenzen oder Farben, verschiedene LEDs ansteuern.
Dank seiner kompakten Bauweise könnte man das Mbed relativ einfach hinter einem Smart-TV anbringen und über den TV-USB Port mit Spannung versorgen. Fügt man nun einige RGB LED-Streifen hinzu erhält man ein eigens hergestelltes Ambilight-Modul zum Nachrüsten.