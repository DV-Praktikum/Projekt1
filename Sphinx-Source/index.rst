.. DVA Bericht documentation master file, created by
   sphinx-quickstart on Wed Oct 11 12:11:12 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============
Versuch 1: Mbed 
===============



.. figure:: img/lpc1768.jfif
   :scale: 100%
   
   Das Mbed-Board *(Quelle: https://elmicro.com/site/assets/files/1381/mbed-nxp-lpc1768.png)*


| Hochschule Augsburg
| Fakultät für Informatik
| DVA Praktikum  (Prof. Dr. Högl)
| Wintersemester 2017/2018

Autoren:

| Johann Roth, <Johann.Roth@hs-augsburg.de>, TI3, #2013011
| Emanuel Stölzle, <Emanuel.Stoelzle@hs-augsburg.de>, TI3, #2006390
| Johannes Maier, <Johannes.Maier2@hs-augsburg.de>, TI3, #2010755

Dieser Text steht unter der Creative Commons Lizenz 
`Namensnennung/Keine kommerzielle Nutzung <http://creativecommons.org/licenses/by-nc/3.0/de>`_


.. figure:: img/cc-logo.jpg
   :scale: 30%
   :align: center


Inhalt:

.. toctree::
   :maxdepth: 2
   :numbered: 

   einleitung
   ziele-anforderungen
   umsetzung
   schluss


Index
=====

* :ref:`genindex`
* :ref:`search`

