Ziele und Anforderungen
=======================

.. _anforderungen:

Online- und Offlinecompiler
---------------------------
Eine Anforderung für unseren ersten Versuch war die Verwendung eines Compilers welcher auch ohne Zugang zum Internet genuzt werden kann. Dies ist nähmlich vor allem in Unternehmen aus Datenschutzgründen wichtig. Denn teilweise werden dort Kunden bedient, welche strenge Geheimhaltungsvorschriften fordern. Zudem kann es wichtig sein, auf eine Offline-Umgebung zurückgreifen zu können, wenn gerade keine Anbindung an das Internet möglich ist. 
Ausführliche Infos über die Handhabung beider Compiler finden Sie unter folgendem Link: :numref:`compiler`.

Programmieren auf dem Mbed
--------------------------
Um das Programmieren auf dem Mbed in Verbingung mit Hardware kenne zu lernen, haben wir diverse Bauteile, wie LEDs und ein Dislplay bekommen.


.. figure:: img/Mbed.jpeg
   :scale: 30%
   :align: center


   Das Mbed-Board


.. figure:: img/LedTaster.jpeg
   :scale: 20%
   :align: center

   Eine Platine mit aufgelöteten LEDs und einem Taster


.. figure:: img/LCD-Display.jpeg
   :scale: 30%
   :align: center

   Ein LCD-Display der DOG-Display-Serie


Programmieren der LEDs
``````````````````````
In Verbindung mit den LEDs haben wir verschiedene Programme implementiert und getestet. 
Das Arbeiten mit LEDs eignet sich hervorragend für den ersten Kontakt mit einer neuen Technologie, da sich Programme sehr einfach impementieren lassen und insgesamt einfach überprüft werden kann ob die Hardware plus die Verschaltung einwandfrei funktionieren.
Die Umsetztung dazu finden Sie hier: 
:numref:`progLed`.

Programmieren des LCD-Displays
``````````````````````````````
Das Programmieren eines LCD-Displays gehörte zu der letzten Anforderung des ersten Praktikum-Versuches. Dabei wurden mehrere Methoden getestet um ein Bild auf dem Display zu erzeugen, jedoch leider ohne Erfolg. 
Mehr dazu erfahren sie im :numref:`progDis`. 
