Umsetzung
=========

.. _compiler:

Compiler
--------
Wie schon erwähnt ist eine der Anforderungen für unseren ersten Versuch die Verwendung eines Offlinecompilers. Um dieses Thema werden wir uns im Folgenden beschäftigen, nachdem wir einen Blick auf den Online-Compiler geworfen haben, mit dem wir auch die ersten Projekte realisierten.

Der Onlinecompiler auf https://os.mbed.com/compiler/
````````````````````````````````````````````````````
Der offizielle Compiler (bzw. Entwicklungsumgebung) von Mbed, erreichbar auf der Seite ``https://os.mbed.com/compiler/`` war der erste Compiler mit dem wir unsere Programmtexte compilierten. Die Anleitung für die ersten Schritte fanden wir auf dem mitgelieferten Handzellel: 

.. figure:: img/anleitung.jpeg
   :scale: 50%
   :align: center

   Der "Setup Guide" für den Microcontroller Mbed NXP LPC1768

Im Folgenden eine Anleitung wie wir im Detail vorgegangen sind:

1. Anschließen der Platine über das mitgelieferte USB-Kabel an eine PC (OS: Windows 10)

#. Öffnen des internen Flashspeichers der Platine mit einem Datei-Explorer

#. Öffnen der Datei ``MBED.html``, welche auf die Seite: https://os.mbed.com/platforms/mbed-LPC1768/ weiterleitet:

.. figure:: img/MBED_html.jpg
   :scale: 80%
   :align: center

   Screenshot der Internetseite
   
4. Anmelden auf der Mbed-Plattform

Wenn man die genannten Arbeitsschritte durchgeführt hat, erhält man über die Schaltfläche ``"Compiler"`` Zugriff auf die Online-Entwicklungsumgebung.
Hier kann man Programmtexte editieren und compilieren. Den compilierten Code (in Form einer BIN-Datei) kann man herunterladen und auf der Platine speichern. Unerlässlich ist es dabei zu überprüfen, dass sich lediglich eine Binärdatei auf der Platine befindet. Durch drücken des ``Reset``-Knopfes wird dann das neue Programm geladen und ausgeführt.

Der Offlinecompiler der PlatformIO
```````````````````````````````````
Es gibt Situationen in denen auf die oben beschriebene Methode nicht zurückgegriffen werden kann. Gründe hierfür wurden in der Anforderungsbeschreibung genannt: :numref:`anforderungen`.
Abhilfe schafft das Arbeiten mit einem Offline-Compiler.
Hier viel unsere Wahl auf die **"PlatformIO IDE"** welche als Erweiterung für die **IDE "Atom"** installiert wird.

Im Folgenden eine Anleitung wie wir im Detail vorgegangen sind (OS: Windows 10). (Hilfe fanden wir auf der Seite: http://docs.platformio.org/en/latest/ide/atom.html):

1. Download er Atom-Installationsdatei unter Öffnen des Links: https://atom.io/download/windows_x64

#. Installation der Atom-IDE

#. Starten von Atom

#. Öffnen des ``Atom Package Manager: Menu: File > Settings > Install``

#. ``"platformio-ide"`` Suchen und bei dem ersten Eintrag der Erweiterungsliste auf ``Install`` klicken. 

Damit ist die Installation der Erweiterung *PlatformIO* in dem *Atom-Editor* abgeschlossen.

.. figure:: img/startAtom.jpg
   :scale: 50%
   :align: center

   Screenshot der Atom-Entwicklungsumgebung

Durch das Klicken auf *New Projekt* kann ein neues Projekt erstellt werden. 

.. figure:: img/ProjektWizard.jpg
   :scale: 110%
   :align: center

   Der Projekt Assistent
   
Dies führt uns zum nächsten Schritt. Entsprechend unserer Platine wählen wir als Board: *NXP mbed LPC1768* und als Framework: *mbed*.
Nach Klicken auf *Finish* wird ein neuer Projektordner *main.cpp* erstellt.

.. figure:: img/neuesProjekt.jpg
   :scale: 60%
   :align: center

   Ein neu angelegtes Projekt

Nun kann der editierte Code durch das Anklicken der "Häckchen-Schaltfläche" gespeichert und Compiliert werden. Will man den Code zudem auf die Platine speichern, klickt man auf die "Pfeil-Schaltfläche". Der Code wird dann in die Datei "firmware.bin" auf der Platine gespeichert. Auch hier muss man wieder darauf achten, dass sich keine andere Binärdatei auf der Platine befindet.


Programmieren
-------------


Inbetriebnahme/Allgemeine Schritte zum Flashen von Software
````````````````````````````````````````````````````````````
Um uns mit der IDE des mbed NXP vertraut zu machen nutzten wir zunächst einmal den im Lieferumfang enthaltenen Setup Guide, welcher beschreibt, wie man so dieser gelangt.
Als ersten Schritt verbindet man den Microcontroller per USB mit einem Rechner. Sobald der mbed vom PC erkannt wurde und die USB-Treiber fertig installiert sind, kann man direkt auf den Flash-Speicher über den Datei-Manager des PCs zugreifen, auf dem die MBED.HTM-Datei liegt. Öffnet man diese per Doppelklick gelangt man direkt zur Entwicklungsplattform, auf der man sich registrieren kann um den Programmcode hier online und zentral gespeichert auf dem Server von NXP zu entwickeln. Alternativ ist die Seite im Internet auf.

Eine ausführlichere Anleitung finden Sie unter: :numref:`compiler` 

Desweiteren bietet die Plattform die Möglichkeit, die mit dem Programmeditor erstellte, Software direkt zu kompilieren.
Die kompilierte .bin-Datei wird im Downloadordner des Benutzers abgespeichert. Von hier aus wird sie mit dem Datei-Manager direkt in das Stamm-Verzeichnis des Microcontrollers verschoben. Nach einem Neustart, welcher entweder durch Aus-und Einstecken des USB-Kabels oder durch das Betätigen des Resett-Buttons erfolgt, wird die neue Software auf den Microcontroller geflasht.

.. _progLed:

Testprogramm 1 - Blinklicht
````````````````````````````
Zu Testzwecken haben wir ein kleines Programm geschrieben, welches eine LED ansteuert und diese in 0,2-Sekündigen Abständen blinken lässt. Hierfür ist es von Nöten die Bibliotheksdatei „mbed.h“ über die IDE einzubinden. Dies geschieht über den den Button „Import“ in der Menüleiste. Im Folgenden bekommt der Benutzer eine große Auswahl an Bibliotheksdateien, die je nach bedarf aus der Bibliothek von mbed.org importiert werden können. Sobald die „mbed.h“-Datei im Programmcode importiert ist stehen alle hier enthaltenen Funktionen zur Verfügung. Die Datei bietet eine große Auswahl an Funktionen, die die Programmierung des Microcontrollers deutlich erleichtern. 

Programmtext:

.. code-block:: c
   :linenos:

    #include "mbed.h"
    DigitalOut myled(LED1);	//Erstellen eines Objekts vom Typ DigitalOut

    int main() {
        while(1) {
            myled = 1;		//LED1 Leuchtet
            wait(0.2);		//Zustand für 0,2 Sekunden abwarten
            myled = 0;		//LED1 wird wieder ausgeschaltet
            wait(0.2);		
        }
    }


.. figure:: img/test1.jpg
   :align: center
   :scale: 120%

   Hardwareaufbau zu Testprogramm 1


Testprogramm 2 - Lichterkette
``````````````````````````````
Nun wollen wir drei LEDs (Rot, Gelb, Grün) ansteuern. Um die Pins richtig zu verbinden haben wir uns die beigelegte Pinbelegung zu Hilfe genommen. 

.. figure:: img/Pinbelegung.jpeg
   :scale: 50%
   :align: center

   Pinbelegung



Programmtext:

.. code-block:: C++
   :linenos:
   
    #include "mbed.h"		//eingebundene Headerdatei

    DigitalOut LedR(p5);	//erzeugen von Objekten vom Typ „DigitalOut“	
    DigitalOut LedY(p6);	
    DigitalOut LedG(p7);

    int main()
     {
        LedR =1;		//Rote LED wird angesteuert

        while(true) 
        {
            wait(0.5f);	//warten für 0,5 Sekunden
            LedR =0;		//Rote LED wird wieder ausgeschalten
            LedY=1;		//Gelbe LED wird angesteuert
            wait(0.5f);
            LedY=0;
            LedG=1;
            wait(0.5f);
            LedG=0;
            LedR=1; 
        }
    }


Aufgefallen ist, dass bei richtiger Verbindung der Pins und der Stromversorgung sowie Ground kein Signal an den LEDs ankam. Schnell fiel bei der Untersuchung der LEDs auf, dass diese falsch eingelötet waren. Nachdem dies umgelötet und korrigiert wurde schien das Problem zunächst beseitigt. Allerdings leuchtete jetzt nur eine der drei LEDs. Nach einer Messung stand fest, die Bauteile waren defekt. Also löteten wir wiederum zwei neue LEDs ein. Jedoch führte auch das noch nicht zum gewünschten Ergebnis, denn die Lämpchen leuchteten nur sporadisch auf, ohne erkennbares Muster. Nun nahmen wir die Lötstellen auf der Rückseite der Vorwiderstände unter die Lupe. Hier konnten wir auf Anhieb erkennen, dass diese schlecht gelötet war, denn wir fanden eine kalte Lötstelle vor. 
Nach all den anfänglichen Hürden konnten wir letztendlich doch noch die Ampel-Lämpchen nacheinander in Reihe leuchten lassen wie in Abbildung 1 zu sehen ist.

.. figure:: img/test2.jpg
   :scale: 130%
   :align: center

   Hardwareaufbau Programm 2


Testprogramm 3 - Lichterkette mit Unterbrechung
```````````````````````````````````````````````````````
In folgenden Programm wurde die Initialisierung der DigitalOut-Objekte mit Hilfe eines Arrays realisiert, sodass sich der Zugriff auf diese komfortabler gestalten lässt. Zudem wird ein Taster auf Pin8 initialisiert. 
Die Hauptfunktion dieses Programmes ist es eine Lichterkette (wie schon bei Testprogramm 2) auf der Platine zu erzeugen. Durch drücken des Taster wird die Lichterkette pausiert.

Programmtext:

.. code-block:: C++
   :linenos:
   
   #include <mbed.h>
    DigitalOut myLeds [4] = {LED1, LED2, LED3, LED4}; //Array mit den auf die Platine aufgelöteten LEDs
    DigitalIn myButton (p8); //Ein an Pin8 angeschlossener Knopf
    int main() {
    myButton.mode(PullUp);
        while(1) {
            int i = 0;
            for(i = 0; i < 4; i++){
                wait(0.05);
                myLeds[i] = 1;
                int a = (i==0)? 3 : i-1;

                myLeds[a] = 0;

                while (!myButton) { // solange der Button gedrückt ist, wird das Programm "pausiert"
            
                }
            }
        }
    }



.. figure:: img/LEDmitTaster.gif
   :scale: 100%
   :align: center
   
   Test des Programmes mit Taster


Testprogramm 3 - Binärzähler
``````````````````````````````
Im folgenden Programm wurde ein Binärzähler realisiert. Das Programm zählt die Tastendrücke und gibt sie auf dem LEDs mit 3 "bit" binär aus. (Nach der Zahl Sieben folgt wieder die Null.)

Programmtext:

.. code-block:: C++
   :linenos:
      
   #include <mbed.h>

   DigitalOut myLeds [4] = {LED1, LED2, LED3, LED4};
   DigitalIn myButton (p8);
   int main() {
   myButton.mode(PullUp);
    int i = 0;
    while(1) {
        if(!myButton){
          i++;
          while (!myButton) {
            wait(0.1);
          }
        }
        i = i %8;

        switch (i) {
          case 0:{
            myLeds[0] = 0; myLeds[1] = 0; myLeds[2] = 0;
            break;
          }
          case 1:
            myLeds[0] = 0; myLeds[1] = 0; myLeds[2] = 1;
            break;
          case 2:
            myLeds[0] = 0; myLeds[1] = 1; myLeds[2] = 0;
            break;
          case 3:
            myLeds[0] = 0; myLeds[1] = 1; myLeds[2] = 1;
            break;
          case 4:
            myLeds[0] = 1; myLeds[1] = 0; myLeds[2] = 0;
            break;
          case 5:
            myLeds[0] = 1; myLeds[1] = 0; myLeds[2] = 1;
            break;
          case 6:
            myLeds[0] = 1; myLeds[1] = 1; myLeds[2] = 0;
            break;
          case 7:
            myLeds[0] = 1; myLeds[1] = 1; myLeds[2] = 1;
            break;
        }

        }
     }



.. figure:: img/binaerzaehler.gif
   :scale: 100%
   :align: center
   
   Testaufbau des Binärzählers

.. _progDis:

Display
```````

Zu Beginn verlöteten wir das Display auf einer Lochplatine um eine einfache Steckverbindung zu den Pins zu gewährleisten. Das vereinfacht die Arbeit bzw. das Ein- und Umpinnen der Verbindungskabel erheblich.
Nachdem die Hintergrundbeleuchtung korrekt nach Datenblatt, mittels Pin 1,2,3 und 4, (siehe unten folgende Abbildung) mit der Spannungsversorgung verbunden wurde, konnten wir auch diesen Test als erfolgreich abhaken.


.. figure:: img/Pinbelegung_dog.jpeg
   :scale: 50%
   :align: center

   Pinbelegung für das Dog-Display (Quelle: https://www.mikrocontroller.net/topic/423401)



Das Display selbst wurde ebenso erst über den 5V Spannungseingang, am Pin 26 für VDD und am Pin 27 für Ground, eingepinnt (S. 8 EA DogM162W-A DisplayDatenblatt).

.. figure:: img/pinbelegung-datasheet.jpeg
   :scale: 50%
   :align: center
   
   Pinbelegung laut Datenblatt (Quelle: http://www.lcd-module.de/pdf/doma/dog-m.pdf#page=8)

Entgegen der Erwartung zeigte das Display noch keine Reaktion. Lediglich mit verbundener Spannungsversorgung hätte es zumindest aufleuchten und die leeren Pixelraster standartmäßig anzeigen müssen. So haben wir verschiedene Möglichkeiten der Pinbelegung um Daten zu übertragen getestet z.B. über das SPI (Serial Peripheral Interface). Nach der, im Lieferumfang enthaltenen Karte mit aufgedruckter Pinbelegung des Mbed, gingen wir entsprechend vor um eine Verbindung über das SPI aufzubauen. Pin 11 bis 13 der SPI-Schnittstelle wurden entsprechend verbunden. Als nächstes Testscenario wollten wir einen Demoprogrammcode, welcher ein "hellombed!" auf dem Display ausgeben sollte, ausführen. 

Programmtext:

.. code-block:: C++
   :linenos:


    #include "DogLCD.h"
    #include "hellombed.h"
 
    SPI spi(p5, NC, p7);
    DogLCD dog(spi, p17, p18, p20, p19);        //  spi, power, cs, a0, reset
  
    int main()
    {
        dog.init();
        // draw "hello mbed"
        dog.send_pic(pic_hellombed);
        wait(5);
    }

.. figure:: img/Pintabelle.JPG
   :scale: 100%
   :align: center
   
   Pinbelegung



Aber auch hier keine Reaktion des Displays. Nach einigen Vergleichen zu anderen Projekten mit baugleichen Modellen aus der Microcontroller-Gemeinde sind wir zu dem Entschluss gekommen, dass das Display womöglich einen technischen Defekt aufweist. Deshalb war es uns leider nicht möglich die Anzeige in unser Projekt einzubinden und zu nutzen. Eine Untersuchung des Displays gäbe weitere Aufschlüsse über die technische Funktionalität. Ferner könnte der vermutete Fehler weiter eingegrenzt und eventuell beseitigt werden. Dafür standen uns aber unglücklicherweise nicht die entsprechenden Messgeräte für weitere Testmessungen zur Verfügung.


