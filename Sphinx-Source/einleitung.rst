Einleitung
==========

Da dies das erste Projekt mit eingebetteten Systemen in unserem Studium ist, möchten wir uns im Folgenden einen Überblick über diese Technologie verschaffen.

.. figure:: img/Mbed.jpeg
      :align: center
      :scale: 20%

      Das Mbed Board.

Motivation
----------

Technische Details
``````````````````
Im Folgenden wird das **mbed NXP LPC 1768 Microcontrollerboard** zur Vereinfachung mit mbed bezeichnet . Das mbed wird von einem *32-bit ARM Cortex-m3* mit *96 MHz* Taktung angetrieben. *32KB Arbeitsspeicher* und 512KB Flash Speicher gehören ebenfalls zur Ausstattung des NXP LPC1768. Mit Hilfe der *40 Pins* bietet das Board Kommunikationsmöglichkeiten via *SPI, Serial, Analog, Ethernet USB und CAN*. Die Kosten bewegen sich zwischen **42 und 75 Euro**. 

Grundlegende Zielsetzung
````````````````````````

Unsere Aufgabe ist es, uns durch experimentelles Arbeiten mit der Technik vertraut zu machen und neue Anwendungen zu erlernen. Hierbei greifen wir auf unser, in den ersten beiden Semestern erworbenes Programmier-Wissen, zurück und lernen zugleich neue Herangehensweisen und Anwendungen  kennen. Beispielsweise nutzen wir die Programmiersprache C++, welche in Kombination mit dem mbed einen guten Einstieg in die Welt der Micocontroller bietet. 
Im Vergleich zu anderen Boards bietet das mbed eine Web-basierte IDE, die es möglich macht, den Compiler flexibel auf verschiedenen Betriebssystemen zu nutzen.
So gelang es uns beispielsweise den Code auf Linux und auf Windows Rechnern zu kompilieren und auf das mbed aufzuspielen.  

Außerdem stellt die Community mit ihrem breit gefärchertem Fachwissen vorgefertigte Bibliotheken bereit, die zu Testzwecken, bzw. entsprechend modifiziert, als Vorlage für das eigene Projekt genutzt werden können. 
Ein weiterer Vorteil des mbed sind die kompakten Abmessungen und der niedrige Energieverbrauch. 
Die Spannungsversorgung des mbed erfolgt über ein micro USB Kabel das einfach am PC angesteckt wird. 
Des Weiteren lernten wir mit hardwarebedingter Problemstellung umzugehen und nutzen auch eine Lötstation um defekte Bauteile bzw. Leitungen wiederherzustellen. 
Schließlich haben mithilfe des mbed drei verschiedene Aufgaben realisieren können. Unter anderem eine blinkende LED, eine Ampelanlage und einen Binärzähler. 
