#include "mbed.h"           //eingebundene Headerdatei

 DigitalOut LedR(p5);        //erzeugen von Objekten vom Typ „DigitalOut“
 DigitalOut LedY(p6);
 DigitalOut LedG(p7);

 int main()
  {
     LedR =1;                //Rote LED wird angesteuert

     while(true)
     {
         wait(0.5f); //warten für 0,5 Sekunden
         LedR =0;            //Rote LED wird wieder ausgeschalten
         LedY=1;             //Gelbe LED wird angesteuert
         wait(0.5f);
         LedY=0;
         LedG=1;
         wait(0.5f);
         LedG=0;
         LedR=1;
     }
 }